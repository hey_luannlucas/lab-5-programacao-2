md
# Laboratorio 5 - Estrutura de dados
Este repositório contém três aplicações de estruturas diferentes implementadas em Java relacionadas ao cálculo da massa total.

## Aplicação 1: Calculando a massa da molécula da proteína
- Essa aplicação é uma implementação de console que calcula a massa total de uma proteína com base em sua sequência de aminoácidos.
- Os aminoácidos e suas massas são representados por um dicionário (HashMap) na classe Proteina.
- O programa permite ao usuário informar o nome de um arquivo de texto contendo a sequência de aminoácidos da proteína.
- O arquivo é lido e, em seguida, o programa calcula a massa total da proteína através da soma das massas dos aminoácidos presentes na sequência.

<div align="center">
  <img src="Assets/Snippet-calcular-massa-1.png" alt="Snippet-calcular-massa-1" width="850" height="600">
</div>

## Aplicação 2: Calculando a massa da molécula da proteína usando Stack
- Essa aplicação é uma variação da aplicação 1, onde a estrutura de dados utilizada para o cálculo da massa da proteína é uma pilha (Stack).
- A pilha é utilizada para armazenar temporariamente as massas dos aminoácidos enquanto o arquivo é lido.
- Após a leitura do arquivo, as massas são somadas para obter a massa total da proteína.
- O uso da pilha na aplicação pode ser útil em casos onde há necessidade de armazenar temporariamente os valores antes de realizar a soma ou processamento final.

<div align="center">
  <img src="Assets/Snippet-calcular-massa-2.png" alt="Snippet-calcular-massa-2" width="850" height="650">
</div>

## Aplicação 3: Calculando a massa da molécula da proteína usando Queue
- Nesta aplicação, utilizamos a estrutura de fila (Queue) para calcular a massa total da proteína.
- Assim como na aplicação 2, a fila é utilizada para armazenar temporariamente as massas dos aminoácidos durante a leitura do arquivo.
- Ao final da leitura, as massas são retiradas da fila e somadas para obter a massa total da proteína.
- O uso da fila pode ser útil em casos onde precisamos processar os valores na ordem em que foram adicionados, como é o caso da sequência de aminoácidos em uma proteína.

<div align="center">
  <img src="Assets/Snippet-calcular-massa-3.png" alt="Snippet-calcular-massa-3" width="850" height="650">
</div>

## Diagrama de classes:
<div align="center">
  <img src="Assets/diagram%20de%20classes.png" alt="Diagrama-de-classes" width="800" height="550">
</div>

## Execução:
Ao rodar a classe Main uma janela irá aparecer e você deverá selecionar o seu arquivo .txt que será validado na classe FileSelector. Os propostos pelo Lab se encontram na pasta Assets, poteina1 e proteina2.
Sinta-se a vontade para testá-los.

<div align="center">
  <img src="Assets/Selecione-o-arquivo.png" alt="Selecione-o-arquivo">
</div>

Após ser validado, será mostrado o total.

<div align="center">
  <img src="Assets/Calculo-proteina2.png" alt="Calculo-proteina2">
</div>

***
<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="Assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>