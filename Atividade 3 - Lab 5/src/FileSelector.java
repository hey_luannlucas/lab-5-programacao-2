import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class FileSelector {
    public static String selecionarArquivoProteina() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Selecione o arquivo de proteína");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Arquivos de texto", "txt");
        fileChooser.setFileFilter(filter);

        int userSelection = fileChooser.showOpenDialog(null);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();

            // Verifica se o arquivo é válido e existe
            if (selectedFile.exists() && selectedFile.isFile()) {
                // Verifica se o arquivo tem extensão ".txt"
                if (selectedFile.getName().toLowerCase().endsWith(".txt")) {
                    // Verifica se o arquivo pode ser lido
                    if (selectedFile.canRead()) {
                        return selectedFile.getAbsolutePath();
                    } else {
                        JOptionPane.showMessageDialog(null, "O arquivo não pode ser lido.", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "O arquivo selecionado não é um arquivo de texto (.txt).", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Arquivo inválido ou inexistente.", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
        return null;
    }
}
