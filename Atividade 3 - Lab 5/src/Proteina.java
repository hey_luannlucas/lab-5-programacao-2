import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class Proteina {
    private HashMap<Character, Double> aminoMassMap;

    public Proteina() {
        aminoMassMap = new HashMap<>();
        // Inicializa o dicionário com as massas dos aminoácidos
        aminoMassMap.put('A', 71.03711);
        aminoMassMap.put('C', 103.00919);
        aminoMassMap.put('D', 115.02694);
        aminoMassMap.put('E', 129.04259);
        aminoMassMap.put('F', 147.06841);
        aminoMassMap.put('G', 57.02146);
        aminoMassMap.put('H', 137.05891);
        aminoMassMap.put('I', 113.08406);
        aminoMassMap.put('K', 128.09496);
        aminoMassMap.put('L', 113.08406);
        aminoMassMap.put('M', 131.04049);
        aminoMassMap.put('N', 114.04293);
        aminoMassMap.put('P', 97.05276);
        aminoMassMap.put('Q', 128.05858);
        aminoMassMap.put('R', 156.10111);
        aminoMassMap.put('S', 87.03203);
        aminoMassMap.put('T', 101.04768);
        aminoMassMap.put('V', 99.06841);
        aminoMassMap.put('W', 186.07931);
        aminoMassMap.put('Y', 163.06333);
    }

    public double calcularMassaProteinaComQueue(String arquivoProteina) throws IOException {
        Queue<Double> fila = new LinkedList<>();
        double massaTotal = 0.0;

        try (BufferedReader reader = new BufferedReader(new FileReader(arquivoProteina))) {
            String linha;
            while ((linha = reader.readLine()) != null) {
                linha = linha.trim();
                for (int i = 0; i < linha.length(); i++) {
                    char amino = linha.charAt(i);
                    if (aminoMassMap.containsKey(amino)) {
                        double massa = aminoMassMap.get(amino);
                        fila.add(massa);
                    } else {
                        System.out.println("Caractere inválido encontrado na proteína: " + amino);
                        return -1.0; // Retorna -1 em caso de caractere inválido
                    }
                }
            }

            // Calcula a massa total a partir da fila
            while (!fila.isEmpty()) {
                massaTotal += fila.poll();
            }
        }

        return massaTotal;
    }
}
