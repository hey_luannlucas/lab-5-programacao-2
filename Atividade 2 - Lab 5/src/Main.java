import javax.swing.JOptionPane;

public class Main {
    public static void main(String[] args) {
        String arquivoProteina = FileSelector.selecionarArquivoProteina();

        if (arquivoProteina != null) {
            Proteina proteina = new Proteina();
            try {
                double massaTotal = proteina.calcularMassaProteinaComStack(arquivoProteina);
                if (massaTotal >= 0.0) {
                    String massaFormatada = String.format("%.3f", massaTotal);
                    JOptionPane.showMessageDialog(null, "Massa total da proteína: " + massaFormatada, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao calcular a massa da proteína.", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ocorreu um erro ao processar o arquivo: " + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nenhum arquivo de proteína selecionado.", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }
}
